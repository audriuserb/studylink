module.exports.saveAnswers = (sessionID, answeredQuestions) => {
    var MyAnswersRepository = require('../repositories/myAnswersRepository');
    return MyAnswersRepository.saveAnswers(sessionID, answeredQuestions)
}