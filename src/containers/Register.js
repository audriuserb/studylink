import {connect} from "react-redux"
import RegisterComponent from "../components/RegisterComponent"
import {fetchUser} from "../actions/registerActions"

const mapStateToProps = ({ logInR }) => {
    return{
        isRegistered: logInR.isRegistered,
   	isChecked: logInR.isChecked
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        onHandleSubmit: (user) =>{
            dispatch(fetchUser(user))
        }
    }
}

const Register = connect(mapStateToProps,mapDispatchToProps)(RegisterComponent)
export default Register