import {REQUEST_USER, SUCCESS_USER_REGISTER, FAILURE_USER_REGISTER} from "../constants"
import fetch from "isomorphic-fetch"

// User Registration
export function fetchUser(user) {
    return dispatch => {
        dispatch(responseUser(user));
    };
}

function requestUser(){
    return{
        type: REQUEST_USER
    };
}

function successUserRegister(isRegistered){
    return{
        type: SUCCESS_USER_REGISTER,
        isRegistered
    }
}

function failureUserRegister(error){
    return {
        type: FAILURE_USER_REGISTER,
        error
    }
}

function responseUser(user){
    var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    return dispatch => {
        dispatch(requestUser())
        fetch('http://193.219.177.19:3101/adduser' , {
        method: "POST",
        headers: {
            'Content-type': 'application/json',
            'CSRF-Token': token
        },
        body: JSON.stringify(user)
        })
        .then(response => response.json())
        .then( isRegistered => {
                dispatch(successUserRegister(isRegistered))
        })
        .catch(error => {
            dispatch(failureUserRegister(error))
        })
    }
}