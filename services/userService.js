module.exports.fetchUser = (id) => {
    var UserRepository = require('../repositories/userRepository');
    return UserRepository.fetchOne(id)
    .then((r)=> {
        if(r == null){
            return false;
        }
        else{
            return true;
        }
    })
}

module.exports.newUser = (userInfo) => {
    var UserRepository = require('../repositories/userRepository');
    return UserRepository.saveUser(userInfo)
}