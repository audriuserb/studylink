import { SUCCESS_QUESTIONS_FETCH, FAILURE_QUESTIONS_FETCH, CANVAS_LOADED,NEXT_QUESTION, CURRENT_QUESTION_LOADED, SUCCESS_QUIZ_RESULT_CALCULATION, FAILURE_QUIZ_RESULT_CALCULATION, QUIZ_RESET} from "../constants"
import fetch from "isomorphic-fetch"

export function fetchQuestions(){
    return dispatch => {
        dispatch(responseQuestions());
    };
}

function successQuestionsFetch(questions){
    return{
        type: SUCCESS_QUESTIONS_FETCH,
        questions
    }
}

function failureQuestionsFetch(error){
    return {
        type: FAILURE_QUESTIONS_FETCH,
        error
    }
}

function responseQuestions(){
    var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    return dispatch => {
        fetch("http://193.219.177.19:3101/getdata", {
                method: 'POST',
                headers: {
                    "Content-Type" : "application/json",
                    'CSRF-Token': token,
            },
            })
            .then(res => res.json())
            .then( questions => {
                dispatch(successQuestionsFetch(questions))
            })
            .catch(error => {
                dispatch(failureQuestionsFetch(error))
            })
    }
}

export function fetchQuizResults(answeredQuestions){
    return dispatch => {
        dispatch(responseQuizResults(answeredQuestions));
    };
}

function successQuizResultCalculation(results){
    return{
        type: SUCCESS_QUIZ_RESULT_CALCULATION,
        results
    }
}

function failureQuizResultCalculation(error){
    return {
        type: FAILURE_QUIZ_RESULT_CALCULATION,
        error
    }
}

function responseQuizResults(answeredQuestions){
    var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    return dispatch => {
        fetch("http://193.219.177.19:3101/checkresult", {
                method: 'POST',
                headers: {
                    "Content-Type" : "application/json",
                    'CSRF-Token': token
                },
                body: JSON.stringify(answeredQuestions)
            },
            )
            .then(res => res.json())
            .then( results => {
                dispatch(successQuizResultCalculation(results))
            })
            .catch(error => {
                dispatch(failureQuizResultCalculation(error))
            })
    }
}

//UTILS
export function fetchCanvas(app){
    return {
        type: CANVAS_LOADED,
        app
    };
}

export function fetchCurrentQuestion(){
    return {
        type: CURRENT_QUESTION_LOADED
    };
}

export function fetchNextQuestion(answer_id){
    return {
        type: NEXT_QUESTION,
        answer_id
    };
}

export function fetchQuizReset(){
    return {
        type: QUIZ_RESET
    };
}