module.exports.fetchResultCoordinates = (id) => {
    var ResultCoordinatesRepository = require('../repositories/resultCoordinatesRepository');
    return ResultCoordinatesRepository.fetchCoordinates(id);
}