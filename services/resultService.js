module.exports.fetchResult = (id) => {
    var ResultRepository = require('../repositories/resultRepository');
    return ResultRepository.fetchOne(id);
}