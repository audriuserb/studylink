var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ItemCoordinatesSchema = new Schema(
  {
      x: {type: Number, required:true},
      y: {type: Number, required:true},
  }
);

var IconCoordinatesSchema = new Schema(
    {
        x: {type: Number, required:true},
        y: {type: Number, required:true},
    }
  );

var ResultsCoordinatesSchema = new Schema(
  {
    picture: {type: String, required: true},
    item_coordinates: [ItemCoordinatesSchema],
    icon_coordinates: [IconCoordinatesSchema],
    entry_text: {type: String, required: true},
    end_text: {type: String, required: true},
  },
  {
    collection: "resultsCoordinates"
  }
);

//Export model
module.exports = mongoose.model('ResultsCoordinates', ResultsCoordinatesSchema);