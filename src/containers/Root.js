import {connect} from "react-redux"
import RootComponent from "../components/RootComponent"
import {fetchCookie,fetchLogOut} from "../actions/cookieActions"


const mapStateToProps = ({ logInR }) => {
    return{
        isRegistered: logInR.isRegistered,
        isChecked: logInR.isChecked,
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        onHandleLoad: () =>{
            dispatch(fetchCookie())
        },
        onHandleLogOut: () =>{
            dispatch(fetchLogOut())
        }
    }
}

const Root = connect(mapStateToProps,mapDispatchToProps)(RootComponent)
export default Root