var ResultService = require('../services/resultService');
var ResultCoordinatesService = require('../services/resultCoordinatesService');
var StudyProgrammeService = require('../services/studyProgrammeService');
var QuestionService = require('../services/questionService');
var MyAnswersService = require('../services/myAnswersService');

    module.exports.calculateResults = async (req) => {
        const programmes = await StudyProgrammeService.fetchStudyProgramme()
        const questions = await QuestionService.fetchQuestions()
        const answeredQuestions = await MyAnswersService.saveAnswers(req.sessionID,req.body)
        const initialProgState = await _getInitial(programmes) 
        await answeredQuestions.answers.forEach( answeredQuestion => {
            questions[answeredQuestion.question_id].answers[answeredQuestion.answer_id].weigth.forEach( weigth => {
                _addResult(initialProgState,weigth)
            })
        })
        let results = {
                result_programme_id: null,
                result_images: {},
                weighty_results: [],
                result_coordinates: {}
        }
        await _getWeightedResults(questions,answeredQuestions,initialProgState,results)
        results.result_images = await ResultService.fetchResult({programme_id: results.result_programme_id})
        results.result_coordinates = await ResultCoordinatesService.fetchResultCoordinates()
        return results
    }

    //pradinių skaiciavimu kintamasis
    function _getInitial(programmes){
        let temp = []
        return new Promise(resolve => {
            programmes.forEach( item =>{ 
            temp[item._id] = 0
        })
        resolve(temp)
          });
    }

    //skaiciavimai
    function _addResult(initialProgState,item){
        return new Promise(resolve => {
            let currentProgrammeState = initialProgState[item.programme_id] +  item.value
            initialProgState[item.programme_id] = currentProgrammeState
            resolve(initialProgState)
        });
    }

    function _getWeightedResults(questions,answeredQuestions,initialProgState,results){
        return new Promise(resolve => {
        resolve(
            answeredQuestions.answers.forEach( answeredQuestion => {
                questions[answeredQuestion.question_id].answers[answeredQuestion.answer_id].weigth.forEach( answer => {
                    if(answer.programme_id == initialProgState.indexOf(Math.max(...initialProgState))){
                        results.weighty_results.push({question_id: questions[answeredQuestion.question_id].questionId, answer_id: answeredQuestion.answer_id}) 
                    }
                })
        }),
            results.result_programme_id = initialProgState.indexOf(Math.max(...initialProgState)),
        )
    });
    }