import {
    SUCCESS_USERS_CHECK,
    REQUEST_COOKIE,
    FAILURE_USERS_CHECK,
    REQUEST_USER,
    SUCCESS_USER_REGISTER,
    FAILURE_USER_REGISTER,
    CLEAR_STORE
} from "../constants"

const initialState = {
    isChecked: false,
    isRegistered: null,
}

function logInReducer(state = initialState, action){
    switch (action.type){
        case SUCCESS_USERS_CHECK:
            return{
                ...state,
                isChecked: true,
                isRegistered: action.isRegistered,
            };
        case REQUEST_COOKIE:
            return{
                ...state,
                isChecking: true
            };
        case FAILURE_USERS_CHECK:
            return{
                ...state,
                error: action.error
            };
        case REQUEST_USER:
            return{
                ...state,
                isChecking: true
            };
        case SUCCESS_USER_REGISTER:
            return{
                ...state,
                isChecked: true,
                isRegistered: action.isRegistered
            };
        case FAILURE_USER_REGISTER:
            return{
                ...state,
                isLoaded: false,
                isChecking: true,
                error: action.error
            };   
        case CLEAR_STORE:
            return {
                isChecked: false,
                isRegistered: false,
            }
        default:
            return state
    }
}

export default logInReducer