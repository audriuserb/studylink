import React,{Component} from 'react'
import { batch } from 'react-redux'
import { BrowserRouter as Router, Route, Link, Redirect  } from 'react-router-dom'
import destroyCanvas from '../canvas/destroyCanvas'
import Register from "../containers/Register"
import Quiz from "../containers/Quiz"
import Home from "../containers/Home"
import { Layout, Menu } from 'antd';
import { UploadOutlined, UserOutlined, HomeOutlined, PlaySquareOutlined, LogoutOutlined  } from '@ant-design/icons';
import About from '../containers/About'

const { Header, Content, Footer, Sider } = Layout;

class RootComponent extends Component{ 
  constructor(props){
    super(props)
    this.onHandleLog = this.onHandleLog.bind(this)
}

  onHandleLog(){
    this.props.onHandleLogOut()
  }

  componentDidMount(){
    if(!this.props.isChecked){
      this.props.onHandleLoad()
  }
  }

  render(){
  return(
   <Router>
   <Layout style={{ minHeight: '100vh' }}>
   <Sider>
      <img src="../images/logo.png" style={{height: 73,'background-repeat': 'no-repeat',margin: 16}}/>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['1']}>
        <Menu.Item key="1" icon={<HomeOutlined />}>
          Namų puslapis
        <Link to="/home"/>
        </Menu.Item>
        {(()=>{
            if(!this.props.isRegistered || this.props.isRegistered == null){
              return ( <Menu.Item key="2" icon={<UserOutlined />}>Registracija<Link to="/register"/></Menu.Item>)
            }
        })()}
        <Menu.Item key="3" icon={<PlaySquareOutlined />}>
          Specialybės testas
          <Link to="/quiz"/>
        </Menu.Item>
        <Menu.Item key="4" icon={<UploadOutlined />}>
          Apie mus
          <Link to="/about"/>
        </Menu.Item>
        {(()=>{
            if(this.props.isRegistered){
              return (<Menu.Item key="5" icon={<LogoutOutlined />} onClick={() => this.onHandleLog()}>Atsijungti</Menu.Item>)
            }
        })()}
      </Menu>
    </Sider>
    <Layout>
      <Header className="site-layout-sub-header-background" style={{ padding: 0 }} />
      <Content style={{ margin: '24px 16px 0' }}>
        <div className="site-layout-background" style={{ padding: 24, minHeight: 360 }}>

              <Route path="/home" component={Home} />
              <Route path="/quiz" component={Quiz} />
              <Route path="/register" component={Register} />
              <Route path="/about" component={About} />
	       <Redirect to="/home"/>
	       </div>
      </Content>
      <Footer style={{ textAlign: 'center' }}>StudyLink ©2020 Sukure Audrius Serbentavičius</Footer>
    </Layout>
  </Layout>
  </Router>)
  }
}
export default RootComponent