import { SUCCESS_USERS_CHECK, FAILURE_USERS_CHECK,CLEAR_STORE} from "../constants"
import fetch from "isomorphic-fetch"

// User Session Check
export function fetchCookie() {
    return dispatch => {
        dispatch(responseCookie());
    };
}

function successCookieResult(isRegistered){
    return{
        type: SUCCESS_USERS_CHECK,
        isRegistered
    };
}

function failureCookieResult(error){
    return {
        type: FAILURE_USERS_CHECK,
        error
    };
}

function responseCookie() {
    var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    return dispatch => {
    return fetch("http://193.219.177.19:3101/checkSession",{
            method: "POST"
            ,
            headers: {
                'Content-type': 'application/json',
                'CSRF-Token': token
            },})
        .then(response => response.json())
        .then( isRegistered => {
                dispatch(successCookieResult(isRegistered))
        })
        .catch(error => {
            dispatch(failureCookieResult(error))
        })
    }
}

export function fetchLogOut() {
    return dispatch => {
        dispatch(responseLogOut());
    };
}

function successSystemReset(){
    return {
        type: CLEAR_STORE,
    };
}

function responseLogOut() {
    var token = document.querySelector('meta[name="csrf-token"]').getAttribute('content')
    return dispatch => {
    return fetch("http://193.219.177.19:3101/logOut",{
            method: "POST"
            ,
            headers: {
                'Content-type': 'application/json',
                'CSRF-Token': token
            },})
            .then(()=>{
                dispatch(successSystemReset())
            })
    }
}