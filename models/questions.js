var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var WeigthSchema = new Schema(
  {
      value: {type: Number, required:true},
      programme_id: {type: Number, required: true}
  }
);

var AnswersSchema = new Schema(
  {
    picture: {type: String, required: true},
    w: {type: Number, required: true},
    h: {type: Number, required: true},
    x: {type: Number, required: true},
    y: {type: Number, required: true},
    type: {type: String, required: true},
    message: {type: String, required: true},
    result_text: {type: String, required: true},
    item_img: {type: String, required: false},
    color: {type: String, required: false},
    weigth: [WeigthSchema]
  }
);

var CharacterSchema = new Schema(
  {
    picture: {type: String, required: true},
    w: {type: Number, required: true},
    h: {type: Number, required: true},
    x: {type: Number, required: true},
    y: {type: Number, required: true},
  }
);

var BaloonSchema = new Schema(
  {
    picture: {type: String, required: true},
    w: {type: Number, required: true},
    h: {type: Number, required: true},
    x: {type: Number, required: true},
    y: {type: Number, required: true},
  }
)

var QuestionSchema = new Schema(
  {
    questionId: {type: Number, required: true},
    question_text: {type: String, required: true},
    picture: {type: String, required: true},
    character: CharacterSchema,
    baloon: BaloonSchema,
    answers: [AnswersSchema]
  },
  {
    collection: "questions"
  }
);

//Export model
module.exports = mongoose.model('Question', QuestionSchema);