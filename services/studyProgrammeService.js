module.exports.fetchStudyProgramme = () => {
    var StudyProgrammeRepository = require('../repositories/studyProgrammeRepository');
    return StudyProgrammeRepository.fetchProgrammes();
}