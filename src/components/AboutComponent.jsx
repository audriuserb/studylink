import React, {Fragment, Component} from "react"
import { Carousel, Divider, Row, Col, Card } from 'antd';
class AboutComponent extends Component{
    constructor(props){
        super(props)
    }

    render(){
    return(
        <Row>
            <Col>
            <Card title="Apie mus" bordered={true} height={500}>
                    <p><b>StudyLink</b> – šiuolaikiškas ir modernus specialybės pasirinkimo testas, skirtas padėti būsimiems Panevėžio kolegijos technologijų fakulteto studentams pasirinkti jiems tinkamiausią studijų programą.</p>
                    <p><b>Tikslinės grupės:</b>
                        <ul>
                            <li>Besimokantys ar dirbantys asmenys, ieškantys tinkamiausios studijų programos.</li>
                            <li>Asmenys, kuriems kyla abejonių dėl studijų programos pasirinkimo.</li>
                            <li>Interesantai, besidomintys Panevėžio kolegijoje vykdomomis studijų programomis.</li>
                            <li>Būsimi studentai, norintys studijuoti Panevežio kolegijoje, technologijų fakultete.</li>
                        </ul>
                    </p>
                    <Divider orientation="left">Panevėžio kolegija</Divider>
                    <p><a href="https://panko.lt/"><img src="https://panko.lt/wp-content/uploads/2015/05/kolegijos_logo.png" height="80px" width="auto"></img></a></p>
                    <p>Panevėžio kolegija yra inovatyvi, aktyvi ir patraukli Lietuvos Respublikos valstybinė aukštoji mokykla, kurioje vykdomos mokslo žiniomis ir praktika grįstos aukštojo mokslo studijos, plėtojami taikomieji moksliniai tyrimai ir (arba) profesionalusis menas. 2016 m. kolegijoje studijavo 1,468 tūkst. studentų.</p>
                    <p>Lietuvos studijų ir mokymo programų registre įregistruotos 23 Panevėžio kolegijos koleginių studijų programos iš socialinių, technologijos, biomedicinos mokslų ir meno studijų sričių. Kolegija mokslo ir kultūros centras: organizuoja seminarus, konferencijas, kvalifikavimo tobulinimo renginius. Kolegija įgyvendina Mokymosi visą gyvenimą principą organizuodama moksleiviams – Karjeros pradžios konsultacijos; Jaunimui – aukštojo mokslo studijų programas; Senjorams – Aukštaitijos savišvietos akademijos renginius.</p>
                    <p>Kolegija suteikia galimybes studentams, dėstytojams ir darbuotojams studijuoti Europos universitetuose; atlikti praktiką ir stažuotis Europos įmonėse.</p>
                    <Divider orientation="left">Autorių teisės</Divider>
                    <p>StudyLink testų sistemos naudojama grafika yra nemokamos licenzijos. Kūrėjas: <a href="http://www.freepik.com">Designed by Freepik</a></p>
                </Card>  
            </Col>
        </Row>
    )
    }
}

export default AboutComponent