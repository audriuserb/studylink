const mongoose = require('mongoose');

const studyProgrammes = new mongoose.Schema({
    _id: {type: Number, required:true},
    programme: {type: String, required:true}
},
{
    collection: "studyProgrammes"
}
)

//Export model
module.exports = mongoose.model('studyProgrammes', studyProgrammes);