import * as PIXI from 'pixi.js'
function loadResults(app, results, questions){
  let item_background_texture, item_background_img, item_texture, mainRes, mainSprite, logoRes, logoSprite, ratio, icon_container, icon_img, icon_count=0, icon_border,baloon_texture, baloon_img, baloon_text
  let item_img = []
  for (var textureUrl in PIXI.utils.BaseTextureCache) {
    delete PIXI.utils.BaseTextureCache[textureUrl];
}
for (var textureUrl in PIXI.utils.TextureCache) {
    delete PIXI.utils.TextureCache[textureUrl];
}
    for (var i = app.stage.children.length - 1; i >= 0; i--) {	app.stage.removeChild(app.stage.children[i])}
    console.log(results)


    let loader = new PIXI.Loader()
    //pasirinktų daiktų nuotraukų įkėlimas
    loader.add('items_result',results.result_coordinates.picture)
    loader.add('baloon','images/baloon_r.png')  
    results.weighty_results.forEach((element, index) => {
      if(questions[element.question_id].answers[element.answer_id].type == "item" || questions[element.question_id].answers[element.answer_id].type == "icon"){
        loader.add('item_'+index,questions[element.question_id].answers[element.answer_id].picture)
      } else {
        loader.add('item_'+index,questions[element.question_id].answers[element.answer_id].item_img)
      }
    })

    // galutinio rezultato įkėlimas
    loader.add('result',results.result_images.picture)
    loader.add('result_logo',results.result_images.logo_img.picture)

    loader.load((loader, resources) => {

    item_background_texture = loader.resources['items_result'].texture
    item_background_img = new PIXI.Sprite(item_background_texture)

    loadBaloon()

    results.weighty_results.forEach((element, index) => {
      item_texture = loader.resources['item_'+index].texture
      if(questions[element.question_id].answers[element.answer_id].type == "item" || questions[element.question_id].answers[element.answer_id].type == "text")
      {
        item_img[index] = new PIXI.Sprite(item_texture)
        item_img[index].anchor.set(0, 1)
        item_img[index].x = results.result_coordinates.item_coordinates[index].x
        item_img[index].y = results.result_coordinates.item_coordinates[index].y
        if(questions[element.question_id].answers[element.answer_id].w > 80 && item_img[index].width > 80){
          ratio = 0.0
          ratio = 1-(questions[element.question_id].answers[element.answer_id].w-80)/questions[element.question_id].answers[element.answer_id].w
          item_img[index].width = questions[element.question_id].answers[element.answer_id].w*ratio
          item_img[index].height = questions[element.question_id].answers[element.answer_id].h*ratio
        }
      } else if (questions[element.question_id].answers[element.answer_id].type == "icon"){
        icon_container = new PIXI.Container();
        icon_img = new PIXI.Sprite(item_texture)
        icon_img.anchor.set(0.5,0.5)
        icon_img.x = results.result_coordinates.icon_coordinates[icon_count].x +73
        icon_img.y = results.result_coordinates.icon_coordinates[icon_count].y +94
        if(questions[element.question_id].answers[element.answer_id].w > 107){
          ratio = 0.0
          ratio = 1-(questions[element.question_id].answers[element.answer_id].w-80)/questions[element.question_id].answers[element.answer_id].w
          icon_img.width = questions[element.question_id].answers[element.answer_id].w*ratio
          icon_img.height = questions[element.question_id].answers[element.answer_id].h*ratio
        }
        icon_border = makeBorders(questions[element.question_id].answers[element.answer_id].color, icon_count)
        icon_container.addChild(icon_border, icon_img)
        item_img[index] = icon_container
        icon_count++
      }
      item_img[index].text = questions[element.question_id].answers[element.answer_id].result_text
    })


    mainRes = loader.resources['result'].texture
    mainSprite = new PIXI.Sprite(mainRes)
    //app.stage.addChild(mainSprite)
    
    function loadBaloon(){
      baloon_texture = loader.resources['baloon'].texture
      baloon_img = new PIXI.Sprite(baloon_texture)
      baloon_img.x = 197
      baloon_img.y = 105
    }

    loadMessage()

    logoRes = loader.resources['result_logo'].texture 
    logoSprite = new PIXI.Sprite(logoRes)
    logoSprite.x = results.result_images.logo_img.x
    logoSprite.y = results.result_images.logo_img.y
    logoSprite.width = results.result_images.logo_img.w
    logoSprite.height = results.result_images.logo_img.h
    //app.stage.addChild(logoSprite)
    init()
    })

    function init(){
      new Promise ((resolve,reject) => {
        app.stage.addChild(item_background_img)
        app.stage.addChild(baloon_img)
        baloonText(results.result_coordinates.entry_text)
        setTimeout(() => {
          resolve()
        }, 5000)
      })
      .then(() =>{
      new Promise ((resolve) => {
        item_img.forEach((element, index) =>{
          setTimeout(() => {
            app.stage.addChild(element)
            baloon_text.text = element.text
          }, 5000*index)
        }),
        setTimeout(() => {
          baloon_text.text = results.result_coordinates.end_text
        }, 5000*item_img.length),
        setTimeout(() => {
          resolve()
        }, 5000*(item_img.length+1))
      })
      .then( () => {
        app.stage.addChild(mainSprite),
        loadMessage(),
        app.stage.addChild(logoSprite)
      })
    })
    }

    function makeBorders(color,index){
      let border = new PIXI.Graphics();
      border.lineStyle(5, 0xFFFFFF)
      border.beginFill("0x"+color, 0.90);
      border.drawRect(results.result_coordinates.icon_coordinates[index].x, results.result_coordinates.icon_coordinates[index].y, 146, 188);
      border.endFill();
      return border
    }

    function baloonText(text){
        baloon_text = new PIXI.Text(text,
        {
          font : 'Arial',
          fontSize: 16,
          fill : 0x000000,
          align : 'center',
          wordWrap: true,
          wordWrapWidth: 150,
          //cacheAsBitmap: true, // for better performance
        }); 
        baloon_text.anchor.set(0.5, 0.5)
        baloon_text.x = baloon_img.width/2
        baloon_text.y = 80
        baloon_img.addChild(baloon_text)
        baloon_img.visible = true
    }

    function loadMessage(){
        const graphics = new PIXI.Graphics();
        graphics.beginFill(0xE0E0E0, 0.90);
        graphics.drawRect(14, 15, 872, 371);
        graphics.endFill();

        let res_text = new PIXI.Text(results.result_images.message,
      {
        font : 'Arial',
        fontSize: 40,
        fill : 0x000000,
        align : 'center',
        wordWrap: true,
        wordWrapWidth: 700,
      }); 
      res_text.anchor.set(0.5, 0.5)
      res_text.x = app.screen.width / 2;
      res_text.y = 300
      graphics.addChild(res_text)
      app.stage.addChild(graphics)
      }
}

export default loadResults
