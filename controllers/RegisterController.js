var UserService = require('../services/userService');
class RegisterController{
    constructor(){
        this.newUser = this.newUser.bind(this)
    }

    newUser(req,res){
        UserService.newUser(req).then((r) =>{
            res.json(r)
        })
    }
}

module.exports = RegisterController
