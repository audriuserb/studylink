module.exports = {
    entry: ["babel-polyfill", "./src/index.js"],
    output:{
        path: __dirname + '/public/js',
        publicPath: __dirname + '/public/js',
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                exclude: /node_modules/,
                use: {
                    loader: "babel-loader"
                }
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.less$/,
                use: [{
                  loader: 'style-loader',
                }, {
                  loader: 'css-loader', // translates CSS into CommonJS
                }, {
                  loader: 'less-loader', // compiles Less to CSS
                 options: {
                   lessOptions: { // If you are using less-loader@5 please spread the lessOptions to options directly
                     modifyVars: {
                       'primary-color': '#1DA57A',
                       'link-color': '#1DA57A',
                       'border-radius-base': '2px',
                     },
                     javascriptEnabled: true,
                   },
                 },
                }]}, 
        ],
    },
    devServer: {
      historyApiFallback: true,
      contentBase: './',
      hot: true
   },
    resolve: {
        extensions: ['*','.js', '.jsx']
    }
}
