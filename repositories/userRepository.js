const User = require('../models/user');

module.exports.fetchOne = (conditions) => {
    return User.findOne(
      conditions )
      .then(Result => {
      //console.log(Result)
      return Result;
    }).catch(err => {
        console.error('Unable to Fetch', err);
        return "error";
    });
};

module.exports.saveUser = (userInfo) => {
  let createdUser = new User({
    firstName: userInfo.body.firstName,
    email: userInfo.body.email,
    schoolName: userInfo.body.schoolName,
    adverts: userInfo.body.adverts,
    sessionId: userInfo.sessionID
})
return createdUser.save().then(() => {
  return true
}).catch(err => {
    console.error('Unable to Fetch', err);
    return "error";
})
}
