import React from 'react';
import {render} from "react-dom"
import Root from './containers/Root'
import mainStore from "./store"
import { Provider } from 'react-redux'

const store = mainStore()

render(
<Provider store={store}>
    <Root/>
</Provider>, 
document.getElementById('app'))