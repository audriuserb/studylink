const ResultsCoordinates = require('../models/resultsCoordinates');

module.exports.fetchCoordinates = () => {
    return  ResultsCoordinates.findOne()
    .then(Result => {
    return Result;
  }).catch(err => {
      console.error('Unable to Fetch', err);
      return "error";
  });
};