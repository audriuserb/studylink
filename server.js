const express = require('express');
const exphbs = require('express-handlebars');
const app = express();
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session')
const mongoose = require('mongoose');
const MongoStore = require('connect-mongo')(session);

app.set('views', __dirname + '/views')
app.use(bodyParser.urlencoded({extended: true}))
app.use(bodyParser.json())

// Registruojame Handlebars view engine
app.engine('hbs', exphbs({defaultLayout: 'main', extname: '.hbs'}))

// Naudojimas Handlebars view engine
app.set('view engine', 'hbs')
app.disable("x-powered-by")
app.use(express.static('public'))
app.use('/images', express.static('images'))

    mongoose.connect('mongodb://admin:EDHJ9jR4jRddJKMY@193.219.177.19:31011/quiz?authSource=admin', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    });
    mongoose.Promise = global.Promise;
    const db = mongoose.connection
    
    app.use(cookieParser());
    app.use(session({
        secret: 'my-secret',
        resave: false,
        saveUninitialized: true,
        cookie: {},
        store: new MongoStore({ mongooseConnection: db, collection: "sessions" })
    }));


//nurodome bet koki porta, siuo atveju 3101
const port = process.env.PORT || 3101;
require('./routes')(app)

// Serveris startuoja
app.listen(port, () => {
    console.log('Serveris paleistas porte: '+ port+ '. Sustabdymui spauskite CTRL ir C');
});