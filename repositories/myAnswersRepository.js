const MyAnswers = require('../models/myAnswers');

module.exports.saveAnswers = (sessionID, answers) => {
    let newMyAnswers = {
      sessionId: sessionID,
      answers: answers
  }
  const filter = {sessionId: sessionID}
  return MyAnswers.findOneAndReplace(filter, newMyAnswers,{new:true, upsert:true, useFindAndModify: false}).then((r) => {
    return r
  }).catch(err => {
      console.error('Unable to Fetch', err);
      return "error";
  })
  }