import * as PIXI from 'pixi.js'
function destroyCanvas(app, dispatchReset){
  app.stop(() => app.destroy(false,true, () => dispatchReset()))
  }
  
  export default destroyCanvas