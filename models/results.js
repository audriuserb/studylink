var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LogoImgSchema = new Schema(
  {
      picture: {type: String, required: true},
      x: {type: Number, required:true},
      y: {type: Number, required:true},
      h: {type: Number, required:true},
      w: {type: Number, required:true},
  }
);

var ResultsSchema = new Schema(
  {
    programme_id: {type: String, required: true},
    picture: {type: String, required: true},
    message: {type: String, required: true},
    logo_img: LogoImgSchema
  },
  {
    collection: "results"
  }
);

//Export model
module.exports = mongoose.model('Results', ResultsSchema);