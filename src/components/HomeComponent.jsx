import React, {Fragment, Component} from "react"
import { Carousel, Divider, Row, Col, Card } from 'antd';
class HomeComponent extends Component{
    constructor(props){
        super(props)
    }

    render(){
    return(
        <Row>
            <Col span={12}>
                <Carousel autoplay>
                    <img class="carousel_img" src="../images/carousel_1.jpg"/>
                    <img class="carousel_img" src="../images/carousel_2.jpg"/>
                </Carousel>
            </Col>
            <Col span={1}></Col>
            <Col span={11}>
                <Card title="StudyLink" bordered={true} height={500}>
                    <p>Sveiki atvykę į Panevėžio kolegijos specialybės pasirinkimo testų sistemą "StudyLink"!</p>
                    <Divider orientation="left">Misija</Divider>
                    <p>Teisingas studijų programos pasirinkimas yra pirmas žingsnis, kalbant apie sėkmę profesiniame kelyje. Atsisveikinus su mokyklos suolu, jaunuolio pečius užgula didžiulė atsakomybė – pasirinkti ne tik jo mėgstamiausią, bet ir tinkamiausią studijų sritį. Dėl nuolat pasikartojančios perteklinės informacijos, būsimiems studentams sunku save įsivaizduoti vienoje ar kitoje studijų programoje. </p>
                    <p>Dėl šios priežasties Panevėžio kolegija sukūrė StudyLink – interaktyvų specialybės pasirinkimo testą, kurio tikslas įvertinti asmens asmens gebėjimus ir pomėgius technologijų srityje ir rekomenduoti labiausiai asmens interesus atitinkančią studijų sritį. </p>
                    <Divider orientation="left">Vizija</Divider>
                    <p>Inovatyvus specialybės pasirinkimo testas, kuriuo lengva ir patogu naudotis – taupantis interesanto laiką ir prieinamas naudojant skirtingą programinę įrangą.</p>
                    <p>Konkretumas – aiškūs, trumpi ir lengvai suprantami testo klausimai ir pasirinkimai, reikalaujantys iš asmens tik reikalingos informacijos.</p>
                    <p>StudyLink – specialybės pasirinkimo vedlys ne tik patars būsimam studentui dėl studijų pasirinkimo, bet  ir suteiks informaciją apie Panevėžio kolegijoje vykdomas studijų programas, bei kur kreiptis stojimo klausimais.</p>
                </Card>
            </Col>
        </Row>
    )
    }
}

export default HomeComponent