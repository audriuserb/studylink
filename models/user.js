var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var userSchema = new Schema(
  {
    firstName: {type: String, required: true},
    email: {type: String, required: true},
    schoolName: {type: String, required: true},
    adverts: {type: Boolean},
    sessionId: {type: String, required: true}
  },
  {
    collection: "users"
  }
);
//Export model
module.exports = mongoose.model('User', userSchema);
