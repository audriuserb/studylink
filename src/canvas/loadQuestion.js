import * as PIXI from 'pixi.js'
function loadQuestion(app,questions,currentQuestion,onHandleQuestionLoad,onHandleQuestionChange){
    let ans_img, ans_texture, baloon_texture, baloon_img, baloon_text, load_texture, load_img, ans_container
  for (var i = app.stage.children.length - 1; i >= 0; i--) {	app.stage.removeChild(app.stage.children[i])}
  for (var textureUrl in PIXI.utils.BaseTextureCache) {
      delete PIXI.utils.BaseTextureCache[textureUrl];
  }
  for (var textureUrl in PIXI.utils.TextureCache) {
      delete PIXI.utils.TextureCache[textureUrl];
  }
    let question = questions.find( object =>{
        if (object.questionId == currentQuestion) return object
      })

    // nuotraukų parsiuntimas iš serverio
    let loader = new PIXI.Loader()
    if(question.picture != ""){
    loader.add('q'+currentQuestion,question.picture)
    }
    loader.add('baloon',question.baloon.picture)     
    loader.add('character',question.character.picture)
    loader.add('loading','images/loading.png')
    //Atsakymų įkėlimas
    for(let i=0; i < question.answers.length; i++){
        loader.add('q'+currentQuestion+'a'+i,question.answers[i].picture)
    }

    //resursų įkėlimas į canvas
    loader.load((loader, resources) => {

      // užkraunamas background
      if(question.picture != "")
      {
      let qTexture = PIXI.Texture.from(loader.resources['q'+currentQuestion].data)
      let qSprite = new PIXI.Sprite(qTexture)
      app.stage.addChild(qSprite)
      }

      ans_container = new PIXI.Container()
      app.stage.addChild(ans_container)

      //užkraunami atsakymai
      for(let i=0; i < question.answers.length; i++){
        if(question.answers[i].type == "icon" && i == 0)
        {
          iconBackground()
        }
        ans_texture = loader.resources['q'+currentQuestion+'a'+i].texture
        ans_img = new PIXI.Sprite(ans_texture)
        ans_img.width = question.answers[i].w
        ans_img.height = question.answers[i].h
        ans_img.x = question.answers[i].x
        ans_img.y = question.answers[i].y
        ans_img.interactive = true
        ans_img.buttonMode = true
        ans_img.id = i
        ans_img.on('mouseup',onClick)
                .on('pointerover', onHover)
                .on('pointerout', onHoverOut);
        app.stage.addChild(ans_img)
      }

      // užkraunamas žmogelis
      let ctexture = PIXI.Texture.from(loader.resources['character'].data)
      let char_img = new PIXI.Sprite(ctexture)
      char_img.height = question.character.h
      char_img.width = question.character.w
      char_img.x = question.character.x
      char_img.y = question.character.y
      app.stage.addChild(char_img)

      //užkraunamas tekstas
      loadMessage()
      loadBaloon()
    })

    function iconBackground(){
      let background = new PIXI.Graphics();
      background.lineStyle(2, 0xffffff, 1);
      background.beginFill("0x"+question.answers[0].color, 1);
      background.drawRect(0, 0, 450, 814);
      background.endFill();
      app.stage.addChild(background)
      background = new PIXI.Graphics();
      background.lineStyle(2, 0xffffff, 1);
      background.beginFill("0x"+question.answers[1].color, 1);
      background.drawRect(450, 0, 450, 814);
      background.endFill();
      app.stage.addChild(background)
    }

    function loadBaloon(){
      baloon_texture = PIXI.Texture.from(loader.resources['baloon'].data)
      baloon_img = new PIXI.Sprite(baloon_texture)
      baloon_img.x = question.baloon.x
      baloon_img.y = question.baloon.y
      baloon_img.width = question.baloon.w
      baloon_img.height = question.baloon.h
      baloon_img.visible = false
      app.stage.addChild(baloon_img)
    }

    function loadMessage(){

      const graphics = new PIXI.Graphics();
      graphics.lineStyle(2, 0xffffff, 1);
      graphics.beginFill(0xE0E0E0, 0.90);
      graphics.drawRect(50, 600, 800, 100);
      graphics.endFill();

      //klausimo tekstas
      let question_text = new PIXI.Text(question.question_text,
    {
      font : '12px Arial',
      fill : 0x000000,
      align : 'center',
      cacheAsBitmap: true, // for better performance
      height: 57,
      width: 82
    }); 
    question_text.x = 80
    question_text.y = 620
    graphics.addChild(question_text)

    //klausimų skaitliukas
    let question_no = new PIXI.Text('Klausimas '+(currentQuestion+1)+'/'+questions.length,
      {
        font : '8px Arial',
        fill : 0x000000,
        align : 'center',
        cacheAsBitmap: true, // for better performance
        height: 57,
        width: 82
      }); 
      question_no.x = 680
      question_no.y = 660
    graphics.addChild(question_no)

      app.stage.addChild(graphics)
    }


    function onClick(e){
      for (var i = app.stage.children.length - 1; i >= 0; i--) {	app.stage.removeChild(app.stage.children[i])}
      let load = new PIXI.Graphics();
      load.beginFill(0xffffff, 1);
      load.drawRect(0, 0, 900, 814);
      load.endFill();
      load_texture = loader.resources['loading'].texture
      load_img = new PIXI.Sprite(load_texture)
      /*load_img.width = 150
      load_img.height = 150*/
      load_img.x = app.screen.width / 2;
      load_img.y = app.screen.height / 2;
      load_img.pivot.x = load_img.width/2
      load_img.pivot.y = load_img.height/2
      app.stage.addChild(load)
      app.stage.addChild(load_img)
      app.ticker.add((delta) => {
        load_img.rotation -= 0.01 * delta;
      });
      onHandleQuestionChange(e.currentTarget.id)
    }

    function onHover(e){
      baloon_text = new PIXI.Text(question.answers[e.currentTarget.id].message,
        {
          font : '8px Arial',
          fill : 0x000000,
          align : 'center',
          wordWrap: true,
          wordWrapWidth: 150,
          //cacheAsBitmap: true, // for better performance
        }); 
        baloon_text.x = baloon_img.width/2 - baloon_text.width / 2
        baloon_text.y = 80 - baloon_text.height / 2
        baloon_img.addChild(baloon_text)
        baloon_img.visible = true
    }
    
    function onHoverOut(){
      baloon_text.text = ''
      app.stage.addChild(baloon_img)
      baloon_img.visible = false
    }

    onHandleQuestionLoad()  
}

export default loadQuestion
