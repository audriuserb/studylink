var UserService = require('../services/userService');

class AppController{
    constructor(){
        this.checkSession = this.checkSession.bind(this)
    }
    
    checkSession(req,res){
        UserService.fetchUser({ sessionId: req.sessionID }).then((r) =>{
            res.json(r)
        })
    }
}

module.exports = AppController