const Results = require('../models/results');

module.exports.fetchOne = (conditions) => {
    //console.log(conditions)
    return Results.findOne(
      conditions )
      .then(Result => {
      //console.log(Result)
      return Result;
    }).catch(err => {
        console.error('Unable to Fetch', err);
        return "error";
    });
};