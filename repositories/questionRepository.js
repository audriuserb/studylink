const Question = require('../models/questions');

module.exports.fetchAll = () => {
    return Question.find()
      .then(Result => {
      return Result;
    }).catch(err => {
        console.error('Unable to Fetch', err);
        return "error";
    });
};
