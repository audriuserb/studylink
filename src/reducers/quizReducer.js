import {
    SUCCESS_QUESTIONS_FETCH,
    FAILURE_QUESTIONS_FETCH,
    CANVAS_LOADED,
    CURRENT_QUESTION_LOADED,
    NEXT_QUESTION,
    SUCCESS_QUIZ_RESULT_CALCULATION,
    FAILURE_QUIZ_RESULT_CALCULATION,
    QUIZ_RESET,
    CLEAR_STORE
} from "../constants"

const initialState = {
    questions: null,
    isCanvasLoaded: false,
    currentQuestion: -1,
    answeredQuestions: [],
    isQuizFinnished: false,
    isResultsLoaded: false,
    results: null,
    app: null,
    reset: false,
    logout: false
}


function quizReducer(state = initialState, action){
switch(action.type){
        case SUCCESS_QUESTIONS_FETCH:
            return{
                ...state,
                questions: action.questions
            };
        case FAILURE_QUESTIONS_FETCH:
            return{
                ...state,
                error: action.error
            };
        case CANVAS_LOADED:
            return{
                ...state,
                isCanvasLoaded: true,
                app: action.app,
                reset: false
            };   
        case CURRENT_QUESTION_LOADED:
            return{
                ...state,
                isCanvasLoaded: false,
            };       
        case NEXT_QUESTION:
            let newAnsweredQuestions
            let newIsQuizFinnished
            let nextQuestion
            if(state.currentQuestion <= 0){
                newAnsweredQuestions = []
                newIsQuizFinnished = false
                nextQuestion  = 0
            } else {
                newAnsweredQuestions = state.answeredQuestions
                newIsQuizFinnished = state.isQuizFinnished
                nextQuestion  = state.currentQuestion
            }
                if(state.currentQuestion+1 >= state.questions.length){
                    newIsQuizFinnished = !state.isQuizFinnished
                    newAnsweredQuestions.push({question_id:state.currentQuestion, answer_id:action.answer_id})
                } else if(state.currentQuestion >= 0){
                    nextQuestion  += 1 
                    newAnsweredQuestions.push({question_id:state.currentQuestion, answer_id:action.answer_id})
                }
            return{
                ...state,
                currentQuestion: nextQuestion,
                answeredQuestions: newAnsweredQuestions,
                isCanvasLoaded: true,
                isQuizFinnished: newIsQuizFinnished
            };
        case SUCCESS_QUIZ_RESULT_CALCULATION:
            return{
                ...state,
                results: action.results
            };
        case FAILURE_QUIZ_RESULT_CALCULATION:
            return{
                ...state,
                error: action.error
            };   
        case QUIZ_RESET:
            return{
                ...state,
                isCanvasLoaded: initialState.isCanvasLoaded,
                currentQuestion: initialState.currentQuestion,
                answeredQuestions: initialState.answeredQuestions,
                isQuizFinnished: initialState.isQuizFinnished,
                isResultsLoaded: initialState.isResultsLoaded,
                results: initialState.results,
                logout: initialState.logout,
                app: initialState.app,
                reset: true

            }  
        case CLEAR_STORE:
            return{
                ...state,
                logout: true
            }  
        default:
            return state
}
}
export default quizReducer