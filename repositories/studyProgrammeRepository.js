const StudyProgrammes = require('../models/studyProgrammes');

module.exports.fetchProgrammes = () => {
    return  StudyProgrammes.find()
    .then(Result => {
    return Result;
  }).catch(err => {
      console.error('Unable to Fetch', err);
      return "error";
  });
};