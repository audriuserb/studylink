var QuestionService = require('../services/questionService');

class QuestionController{
    constructor(){
        this.getQuestions = this.getQuestions.bind(this)
    }

    getQuestions(req,res){
        QuestionService.fetchQuestions().then((r) =>{
            res.json(r)
        })
    }
}
module.exports = QuestionController