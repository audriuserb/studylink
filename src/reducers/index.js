import { combineReducers } from "redux"
import logInReducer from "./logInReducer"
import quizReducer from "./quizReducer"


const allReducers = combineReducers({
    logInR: logInReducer,
    quizR: quizReducer
})

export default allReducers