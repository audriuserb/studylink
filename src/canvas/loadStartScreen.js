import * as PIXI from 'pixi.js'
function loadStartScreen(app,isRegistered,onHandleQuestionLoad,onHandleQuestionChange){
    let background_texture, background_img, btn_texture, btn_img
    // nuotraukų parsiuntimas iš serverio
    let loader = new PIXI.Loader()
    for (var textureUrl in PIXI.utils.BaseTextureCache) {
      delete PIXI.utils.BaseTextureCache[textureUrl];
    }
    for (var textureUrl in PIXI.utils.TextureCache) {
      delete PIXI.utils.TextureCache[textureUrl];
    }
    if(isRegistered){
        loader.add('background',"images/start_screen.png")
        loader.add('button',"images/start_btn.png")

    }else {
        loader.add('register',"images/register.png")
    }

    

    //resursų įkėlimas į canvas
    loader.load((loader, resources) => {

      // užkraunamas background
      if(isRegistered)
      {
        background_texture = loader.resources['background'].texture
        background_img = new PIXI.Sprite(background_texture)
        app.stage.addChild(background_img)
        btn_texture = loader.resources['button'].texture
        btn_img = new PIXI.Sprite(btn_texture)
        btn_img.x = 221
        btn_img.y = 581
        btn_img.interactive = true
        btn_img.buttonMode = true
        btn_img.id = 0
        btn_img.on('mouseup',onClick)
        app.stage.addChild(btn_img)
      } else {
        background_texture = loader.resources['register'].texture
        background_img = new PIXI.Sprite(background_texture)
        app.stage.addChild(background_img)
      }
    })

    function onClick(e){
      for (var i = app.stage.children.length - 1; i >= 0; i--) {	app.stage.removeChild(app.stage.children[i])}
      onHandleQuestionChange(e.currentTarget.id)
    }
    onHandleQuestionLoad()  
}

export default loadStartScreen
