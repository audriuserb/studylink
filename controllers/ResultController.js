var ResultCalculationService = require('../services/resultCalculationService');

class ResultController{
    constructor(){
        this.checkSum = this.checkSum.bind(this)
    }

    async checkSum(req,res){
        ResultCalculationService.calculateResults(req).then((r) =>{
            res.json(r)
        })
    }

}
module.exports = ResultController