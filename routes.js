const RegisterController = require ('./controllers/RegisterController');
const registerController = new RegisterController();
const QuestionController = require ('./controllers/QuestionController');
const questionController = new QuestionController();
const AppController = require ('./controllers/AppController');
const appController = new AppController();
const ResultController = require ('./controllers/ResultController');
var csrf = require('csurf')
const resultController = new ResultController();
var csrfProtection = csrf({ cookie: true })

module.exports = (app) =>{    
    app.get('/*',csrfProtection,function(req,res,next){
        res.render('home',{ csrfToken: req.csrfToken()})
    });
    app.post('/checkSession',csrfProtection, appController.checkSession);
    app.post('/adduser',csrfProtection, registerController.newUser);
    app.post('/getdata',csrfProtection, questionController.getQuestions);
    app.post('/checkresult',csrfProtection, resultController.checkSum);
    app.post('/logOut',csrfProtection, (req,res,next) =>{
        res.clearCookie('connect.sid').status(200).send('Ok.');
    });
    return app
}
