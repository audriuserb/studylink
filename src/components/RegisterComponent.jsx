import React,{Component} from "react"
import { Form, Input, InputNumber, Button, Alert, Checkbox } from 'antd';
import { Redirect  } from 'react-router-dom'

const layout = {
  labelCol: { span: 2 },
  wrapperCol: { span: 4 },
};

const validateMessages = {
  required: 'laukas ${label} yra privalomas!',
  types: {
    email: '${label} nėra teisingas el. pašto adresas!',
    number: '${label} is not a valid number!',
  },
};

class Register extends Component{
    constructor(props){
        super(props)
        this.isFormDisabled = false
        this.alert = {
          message: '',
          description: '',
          type: ''
        }
        this.onFinish = this.onFinish.bind(this);
    }

    onFinish(values){
        this.props.onHandleSubmit(values.user);
    }

    render(){
        if(this.props.isRegistered){
            this.isFormDisabled = true
            this.alert.type = 'warning'
            this.alert.message = 'Pranešimas'
            this.alert.description = "Jūs jau esate prisiregistravęs."
        } else {
            this.isFormDisabled = false
            this.alert.type = 'info'
            this.alert.message = 'Registracija'
            this.alert.description = "Įveskite savo vardą, el. pašto adresą ir mokyklos pavadinimą, jei sutinkate gauti reklaminius laiškus pažymėkite žymimąjį langelį."
        }
	if(this.props.isRegistered == true && this.props.isChecked == true)
	{
			return(<Redirect to="/quiz"/>)
	} else {
        return(
            <div>
            <Alert style={{marginBottom: '50px'}} message={this.alert.message} description={this.alert.description} type={this.alert.type}/>
            <Form {...layout} name="nest-messages" onFinish={this.onFinish} validateMessages={validateMessages}>
              <fieldset disabled = {this.isFormDisabled}>
            <Form.Item name={['user', 'firstName']} label="Vardas" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'email']} label="El. paštas" rules={[{ type: 'email', required: true }]}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'schoolName']} label="Mokykla" rules={[{ required: true }]}>
              <Input />
            </Form.Item>
            <Form.Item name={['user', 'adverts']} valuePropName="checked" wrapperCol={{ ...layout.wrapperCol, offset: 2 }}> 
              <Checkbox>
                  Sutinku gauti reklamas.
              </Checkbox>
            </Form.Item>
            <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 2 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
            </fieldset>
          </Form>
          </div>
        )
	}
    }


}

export default Register