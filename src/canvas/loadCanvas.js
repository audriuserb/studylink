import * as PIXI from 'pixi.js'

function loadCanvas(onHandleCanvasLoad){
  const canvas = document.getElementById('c')
  const app = new PIXI.Application({
    view: canvas,
    width: 900,
    height: 814,
    resolution: devicePixelRatio,
    autoDensity: true
  })
  for (var textureUrl in PIXI.utils.BaseTextureCache) {
    delete PIXI.utils.BaseTextureCache[textureUrl];
  }
  for (var textureUrl in PIXI.utils.TextureCache) {
    delete PIXI.utils.TextureCache[textureUrl];
  }
  onHandleCanvasLoad(app)
}

export default loadCanvas