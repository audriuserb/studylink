import React from 'react';
import loadCanvas from '../canvas/loadCanvas'
import loadQuestion from '../canvas/loadQuestion';
import loadResults from '../canvas/loadResults';
import destroyCanvas from '../canvas/destroyCanvas'
import loadStartScreen from '../canvas/loadStartScreen'
import { Divider, Row, Col, Card, Button } from 'antd';

class QuizComponent extends React.Component{
    constructor(props){
        super(props)
        this.handleQuizReset = this.handleQuizReset.bind(this)
    }

    handleQuizReset(){
      if(this.props.app != null){
        destroyCanvas(this.props.app,this.props.onHandleQuizReset())
      }
    }

  componentDidMount(){
    if(this.props.questions == null){
      this.props.onHandleLoad()
    }
    loadCanvas(this.props.onHandleCanvasLoad)
  }

  render(){
    if(this.props.reset == true && this.props.app == null){
      loadCanvas(this.props.onHandleCanvasLoad)
    }
    if(this.props.logout == true){
      this.handleQuizReset()
    }
    if(this.props.isCanvasLoaded && this.props.currentQuestion < 0 && this.props.isRegistered != null)
    {
      loadStartScreen(this.props.app,this.props.isRegistered,this.props.onHandleQuestionLoad,this.props.onHandleQuestionChange)
    } 
    if( this.props.isCanvasLoaded && this.props.app != null && this.props.currentQuestion >= 0 && this.props.questions != null)
    {
        if(!this.props.isQuizFinnished){
          loadQuestion(this.props.app,this.props.questions,this.props.currentQuestion,this.props.onHandleQuestionLoad,this.props.onHandleQuestionChange)
        }
        else 
        {
          if(this.props.results != null){
            loadResults(this.props.app, this.props.results, this.props.questions)
          }
          else if(this.props.results == null && this.props.currentQuestion+1 == this.props.questions.length) 
          {
            this.props.onHandleQuizFinnish(this.props.answeredQuestions)
          }
      }
    }
    return(
      <Row>
        <Col span={13} style={{minWidth: 800}}>
          <canvas id="c"></canvas>
        </Col>
        <Col span={1}></Col>
            <Col span={8}>
                <Card title="Specialybės testas" bordered={true} height={500}>
                    <p>Šiame puslapyje spręsdami testą išsiaiškinsite, kokia studijų programa jums labiausiai tinka.</p>
                    <Divider orientation="left">Paaiškinimas</Divider>
                    <ul>
                      <li>Testą galima spręsti tik, jei esate prisiregistravę prie sistemos.</li>
                      <li>Testo metu lango apačioje bus pateikiami klausimai, į kuriuos turite atsakyti paspausdami atitinkamą paveiksliuką.</li>
                      <li>Išsprendus testą pateikiamas rezultatas - trumpas filmukas, kuris nurodo svariausius atsakymus ir atitinkamą studijų programą.</li>
                    </ul>
                    <Divider orientation="left">Bandyti testą iš naujo.</Divider>
                    <p>Jei norite testą spręsti iš naujo spauskite mygtuką "Bandyti iš naujo"</p>
                    <Button type="reset" onClick={() => this.handleQuizReset()}>Bandyti iš naujo</Button>
                </Card>
            </Col>
      </Row>
    )
  }
}
export default QuizComponent;