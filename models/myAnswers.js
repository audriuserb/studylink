var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var AnswerSchema = new Schema(
  {
    question_id: {type: Number, required: true},
    answer_id: {type: Number, required:true }
  },
  { _id : false }
);

var MyAnswerSchema = new Schema(
  {
    sessionId: {type: String, required:true},
    answers: [AnswerSchema]
  },
  {
    versionKey: false
  },
  {
    collection: "myAnswers"
  }
);

//Export model
module.exports = mongoose.model('MyAnswer', MyAnswerSchema);