import {connect} from "react-redux"
import QuizComponent from "../components/QuizComponent"
import {fetchQuestions, fetchCanvas,fetchCurrentQuestion, fetchNextQuestion, fetchQuizResults, fetchQuizReset} from "../actions/quizActions"

const mapStateToProps = ({ quizR, logInR }) => {
    return{
        questions: quizR.questions,
        isCanvasLoaded: quizR.isCanvasLoaded,
        currentQuestion: quizR.currentQuestion,
        answeredQuestions: quizR.answeredQuestions,
        isQuizFinnished: quizR.isQuizFinnished,
        results: quizR.results,
        app: quizR.app,
        reset: quizR.reset,
        logout: quizR.logout,
        isRegistered: logInR.isRegistered
    }
}

const mapDispatchToProps = dispatch =>{
    return{
        onHandleLoad: () =>{
            dispatch(fetchQuestions())
        },
        onHandleCanvasLoad: (app) =>{
            dispatch(fetchCanvas(app))
        },
        onHandleQuestionLoad: ()=>{
            dispatch(fetchCurrentQuestion())
        },
        onHandleQuestionChange: (answer_id)=>{
            dispatch(fetchNextQuestion(answer_id))
        },
        onHandleQuizFinnish: (answeredQuestions) => {
            dispatch(fetchQuizResults(answeredQuestions))
        },
        onHandleQuizReset: () => {
            dispatch(fetchQuizReset())
        }
    }
}

const Quiz = connect(mapStateToProps,mapDispatchToProps)(QuizComponent)
export default Quiz